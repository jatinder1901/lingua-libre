from django.db import models


class UploadBatch(models.Model):
    locutor = models.ForeignKey("locutors.Locutor", on_delete=models.CASCADE, related_name="upload_batches")
    language_qid = models.CharField(max_length=32)
    created_time = models.DateTimeField(auto_now_add=True)
    started_time = models.DateTimeField(null=True, blank=True)
    finished_time = models.DateTimeField(null=True, blank=True)
    # TODO: Add constraint (finished > start)?


class Recording(models.Model):
    def get_batch_path(instance, filename):
        return 'batches/{0}/{1}'.format(instance.batch.id, instance.transcription)

    class State(models.TextChoices):
        PENDING = 'pending', 'Pending'
        UPLOADING = 'uploading', 'Uploading'
        UPLOADED = 'uploaded', 'Uploaded'
    
    batch = models.ForeignKey("UploadBatch", on_delete=models.CASCADE, related_name="recordings")
    file = models.FileField(upload_to=get_batch_path, max_length=100) # TODO make sure these are audio/video files
    transcription = models.TextField() # TODO: Make sure it is compatible with UTF-encoded texts
    state = models.CharField(max_length=16, choices=State.choices, default=State.PENDING)
