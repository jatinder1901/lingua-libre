from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, parsers
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from upload_batches.models import UploadBatch, Recording
from user_profile.models import User
from upload_batches.serializers import UploadBatchSerializer, RecordingSerializer


class MyUploadBatchesList(APIView):
    """
    List all upload-batches of the logged-in user
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        # get all locutors of this user
        locutors = User.objects.get(id=request.user.id).locutors.all()
        # get all upload batches for any locutor of this user
        upload_batches = UploadBatch.objects.filter(locutor__in=locutors)
        serializer = UploadBatchSerializer(upload_batches, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        serializer = UploadBatchSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def get_my_upload_batch_or_not_found(request, id):
    """ 
    Gets the Upload Batch of the given ID if it is one of any of the logged-in user's locutors.
    Otherwise, raises a NotFound exception.
    """
    # get all locutors of this user
    locutors = User.objects.get(id=request.user.id).locutors.all()
    # get all upload batches for any locutor of this user
    try:
        return UploadBatch.objects.filter(locutor__in=locutors).get(id=id)
    except ObjectDoesNotExist:
        raise NotFound()


class MyUploadBatch(APIView):
    """
    Manages this particular upload-batch of the logged-in user.
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, id, format=None):
        batch = get_my_upload_batch_or_not_found(request, id)
        serializer = UploadBatchSerializer(batch)
        return Response(serializer.data)


class MyUploadBatchRecordings(APIView):
    """
    Manages recordings for this particular upload-batch of the logged-in user.
    """
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]
    parser_classes = (parsers.MultiPartParser, parsers.JSONParser)

    def get(self, request, id, format=None):
        batch = get_my_upload_batch_or_not_found(request, id)
        # get all recordings for this upload batch
        recordings = Recording.objects.filter(batch=batch)
        serializer = RecordingSerializer(recordings, many=True)
        return Response(serializer.data)

    def post(self, request, id, format=None):
        """
        Upload a recording for this upload batch
        """
        batch = get_my_upload_batch_or_not_found(request, id)
        context = { 'batch': batch }
        serializer = RecordingSerializer(data=request.data, context=context)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
