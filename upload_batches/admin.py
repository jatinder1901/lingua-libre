from django.contrib import admin
from upload_batches.models import UploadBatch, Recording


class UploadBatchAdmin(admin.ModelAdmin):
    pass


class RecordingAdmin(admin.ModelAdmin):
    pass


admin.site.register(UploadBatch, UploadBatchAdmin)
admin.site.register(Recording, RecordingAdmin)
