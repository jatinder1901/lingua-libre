describe('Home page', () => {
  it('can go to the record wizard', () => {
    cy.visit('/')

    cy.get('[data-testid="link-to-recordwizard"]').click()

    cy.url().should('include', '/record')
  })

  it.skip('can login', () => {
    // FIXME: find a way to handle/bypass the OAuth
    cy.visit('/')

    cy.get('[data-testid="login"]').click()
  })
})