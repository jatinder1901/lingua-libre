import { describe, it, expect, beforeEach, beforeAll, afterAll } from 'vitest'
import { setActivePinia, createPinia } from 'pinia'
import { flushPromises } from '@vue/test-utils'
import { setupServer } from 'msw/node'
import { http, HttpResponse } from 'msw'
import { useLabelsStore } from './labels'

const handlers = [
  http.get('https://www.wikidata.org/wiki/Special:EntityData/Q150.json', () => {
    return HttpResponse.json({
      entities: {
        Q150: {
          labels: {
            fr: {
              language: 'fr',
              value: 'français',
            },
            en: {
              language: 'en',
              value: 'French',
            },
          },
        },
      },
    })
  }),
]

const server = setupServer(...handlers)

describe('Labels store', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
    server.resetHandlers() // Reset handlers after each test `important for test isolation`

    const store = useLabelsStore()
    store.labels = {} // since this store uses VueUse's useStorage(), we have to manually clean it between tests
  })

  // Start server before all tests
  beforeAll(() => server.listen({ onUnhandledRequest: 'error' }))

  //  Close server after all tests
  afterAll(() => server.close())

  describe('fetchLabelForWikidataItem action', () => {
    it('should get the French (fr) label for wikidata item Q150 (French)', async () => {
      const store = useLabelsStore()
      store.fetchLabelForWikidataItem('Q150', 'fr')
      await flushPromises()

      expect(store.labels).toStrictEqual({ Q150: 'français' })
    })

    it('should get the English (en) label for wikidata item Q150 (French)', async () => {
      const store = useLabelsStore()
      store.fetchLabelForWikidataItem('Q150', 'en')
      await flushPromises()

      expect(store.labels).toStrictEqual({ Q150: 'French' })
    })

    it('should not get the label of a wikidata item it already has in store', async () => {
      const store = useLabelsStore()
      store.labels = { Q150: 'pomme de terre' }

      store.fetchLabelForWikidataItem('Q150', 'fr')
      await flushPromises()

      expect(store.labels).toStrictEqual({ Q150: 'pomme de terre' })
    })

    it('should set to undefined if the language is not available', async () => {
      const store = useLabelsStore()
      store.fetchLabelForWikidataItem('Q150', 'eo') // there is no 'eo' label in the response crafted above
      await flushPromises()

      expect(store.labels).toStrictEqual({ Q150: undefined })
    })
  })
})
