import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import SoundMeter from './SoundMeter.vue'

describe('rendering tests', () => {
  it('should have 15 sound indicators', () => {
    const wrapper = mount(SoundMeter)
    expect(wrapper.findAll('li').length).toBe(15)
  })

  it('should have all sound indicators as saturated if is saturated', async () => {
    const wrapper = mount(SoundMeter, {
      props: {
        saturated: false,
      },
    })

    expect(wrapper.findAll('li.saturated').length).toBe(0)

    await wrapper.setProps({ saturated: true })

    expect(wrapper.findAll('li.saturated').length).toBe(15)
  })

  it('should have the correct amount of sound indicators active', async () => {
    const wrapper = mount(SoundMeter, {
      props: {
        level: 0,
      },
    })

    expect(wrapper.findAll('li.active').length).toBe(0)

    await wrapper.setProps({ level: 4 })

    expect(wrapper.findAll('li.active').length).toBe(4)

    // test with maxing out the level to a high value
    await wrapper.setProps({ level: 21 })

    expect(wrapper.findAll('li.active').length).toBe(15)

    // and back to 0
    await wrapper.setProps({ level: 0 })

    expect(wrapper.findAll('li.active').length).toBe(0)
  })
})
