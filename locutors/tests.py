from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from locutors.models import Locutor, UsesLanguage, LanguageProficiency
from locutors.serializers import UsesLanguageSerializer
from model_bakery import baker
from user_profile.models import User

LOCUTOR_MODEL_NAME = 'locutors.Locutor'

class AddNewLanguage(APITestCase):
    """
    Tests creating new languages
    """

    def setUp(self):
        self.locutor = baker.make(LOCUTOR_MODEL_NAME)
        self.user = User.objects.get(id=self.locutor.linked_user.id)

    def test_create_new_language(self):
        """
        Creating a new language should store it in the db and send back the data (especially the id).
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('locutors_my_single_languages', kwargs={'id': self.locutor.id})
        data = {
            "language_qid": "Q150",
            "proficiency": LanguageProficiency.NATIVE,
            "place_of_learning": "Q142",
        }
        response = self.client.post(url, data)

        # get data from db
        languages = UsesLanguage.objects.filter(locutor=self.locutor).first()
        serializer = UsesLanguageSerializer(languages)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_not_found_locutor_not_owned(self):
        """
        Trying to create with a locutor owned by another user should return 404.
        """
        # Create another locutor from scratch (so with another user)
        other_locutor = baker.make(LOCUTOR_MODEL_NAME)

        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('locutors_my_single_languages', kwargs={'id': other_locutor.id})
        response = self.client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_not_found_invalid_locutor(self):
        """
        Trying to create with a non-existent locutor should return 404.
        """
        # get API response
        self.client.force_authenticate(user=self.user)
        url = reverse('locutors_my_single_languages', kwargs={'id': 1551})
        response = self.client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_not_authenticated(self):
        """
        Not-authenticated should return 403
        """
        # get API response
        url = reverse('locutors_my_single_languages', kwargs={'id': self.locutor.id})
        response = self.client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
