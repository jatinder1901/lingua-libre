from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from locutors import views

urlpatterns = [
    path('', views.LocutorList.as_view()),
    path('<int:id>', views.LocutorDetail.as_view()),
    path('my', views.MyLocutorList.as_view()),
    path('my/<int:id>', views.MyLocutorDetail.as_view()),
    path('my/<int:id>/languages', views.MyLocutorLanguages.as_view(), name='locutors_my_single_languages'),
]

urlpatterns = format_suffix_patterns(urlpatterns)