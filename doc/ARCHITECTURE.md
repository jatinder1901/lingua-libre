# Architecture

Lingua Libre is a Vue.js application served by a Django-based REST API (using the Django REST Framework).

Both components communicate with various Wikimedia-provided APIs.
In particular, the Vue app fetches labels from the Wikidata API; the REST API interfaces with both Wikimedia Commons' API and OAuth provider to handle authentication and uploads of the recordings.

![](diagrams/generic_architecture.svg)

## Key Concepts
Learn more about the Key Concepts of Lingua Libre in the following pages:
* [Locutors](./LOCUTORS.md)

## Front-end
Vue.js app with the following "pages":
* Homepage
* My Locutors
* RecordWizard

## Back-end
The whole website configuration is located in the Django project's folder: [`lingualibre`](/lingualibre/).

Lingua Libre is then split into various Django apps, each handling their respective data models and API routes:
* User Profile ([`user_profile`](/user_profile/)): manages data about the user's profile
* Locutors ([`locutors`](/locutors/)): manages data about the locutors
* Upload Batches ([`upload_batches`](/upload_batches/)): manages the upload batches of recordings

### REST API
[OpenAPI Specification](openapi.yaml)

### Database
Full specification of the database.
It focuses on the barebone features required to have a functional Record Wizard: User Management, Locutors, Uploads (to Commons) Management, and (possibly) Languages Management.
It will be updated throughout development as more features enter the planned sprints.

**Locutors Management** matches closely the existing Lingua Libre architecture: Users can have as many Locutors as they wish, and each Locutor can speak/sign as many Languages as they wish. No much more thought was put into that.

**User Management** is mostly handled by the Social Auth (`social_django`) Django app. It has a bunch of requirements that the User Model must fulfill. But it can also gather additional data from the OAuth provider (e.g. user groups on the wiki). For now, this is a very barebone implementation.

**Uploads (to Commons) Management** is where much more is happening: as we are no longer on MediaWiki, we no longer have the UploadStash. Therefore, it has to be "sort of" reimplemented. Additionally, I took this opportunity to design an architecture that will simplify the integration of a patrol tool and circumvent the 380 files per 72 minutes limit of Commons: once reviewed in the RecordWizard, an "Upload Batch" is created, with "Recordings" corresponding to each recording. Each record is then uploaded to Lingua Libre's server (in a UploadStash-similar fashion). Uploads to Commons, errors, retries and backoff are thus automatically handled on the server-side. Upload batches are meant to be temporary: once finished (and over a certain grace period?), they are deleted from Lingua Libre's database.

![](diagrams/db_full.svg)

Each folder corresponds to a Django app, and they contain all the Models of their respective app.

Cardinalities are to be read as the following:
`A (1) -- (*) B` is *A has one (1) B, B can have any amount of A*.
