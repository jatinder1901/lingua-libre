from django.db import models
from django.contrib.auth.models import AbstractUser
from locutors.models import Locutor, UsesLanguage, LanguageProficiency

class User(AbstractUser):
    # On creation, a User should have its main locutor created
    def save(self, *args, **kwargs):
        created = not self.pk # returns True if this is a creation, False if this is an update
        super().save(*args, **kwargs)
        if created:
            locutor = Locutor.objects.create(name=self.username, linked_user=self, is_main_locutor=True)
            # Create a default language: French
            UsesLanguage.objects.create(locutor=locutor, language_qid="Q150", proficiency=LanguageProficiency.NATIVE)
